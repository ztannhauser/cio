#include <library.h>
#include "localinclude.h"

int global_int;
double global_double_A, global_double_B;
bool global_bools [ 10 ];
extern whatever global_whatever, *whateverptr;

// I'm a one-line comment!

/* I'm a multiple-line comment,
   I can go on forever!         */

struct A {int a, b, c; double f, g, h;};
union A {enum {A, B, C} H; struct {double a, b;}; struct {char* c;};};
extern struct {float a, b, c;} global_struct;
typedef struct {float a, b, c;} global_type;

type funcname(paramtype paramname1, anothertype paramname2);

type aejdwodijawd(paramtype paramname1, ...) {return 4;}

void main()
{
	type name;
	type* name1, **name2, name3[10];
	varname = 1 + 2;
	a = b + c * d / f >> (q << w) & 2 % awd;
	2345;
	3.45;
	'4';
	"literal_string";
	z = (*(&(asd) + 345) = asda = awdjaoij = awdjoaij) / 5;
	a = 3;
	a += 4;
	a -= 5;
	a *= 6;
	a /= 7;
	a %= 7;
	{
		x = (b >= 3, c <= 4, d == e, e != f == f);
	}
	while(3, 4, 5, 6)
	{
		type t = 5 >> 4 << ****a;
	}
	void fib(int x) {return x <= 1 ? fib(x - 1) + fib(x - 2) : 1;}
	for(int a = 0, b = 1, c = 2;a > b && b > c;a--, b++, c--)
	for(;;) // indefinite
	{
		switch(&adw + 4)
		{
			case 2:
			case '3':
			{
				break;
			}
			default:
			{
				***wqweqwe = &qejqowiej + 323.45;
			}
		}
		break;
		continue;
	}
	{{{{int i = 3;}}}}
	return;
	return 3 + 4 + 5 + adawd + awdjoij;
}

struct {char c, d;} another_func(char c[100], ...)
{
	(cast (char) 45, cast (char*) 0x34,
	cast (int) 3.4, cast (double) 0x34454);
	char a[3] = {'3', 3 + '4', awawdoi};
	z = cast (struct a) {'f', X, 3 + 4 * 5};
	z = cast (struct {char* c, *d;}) {"qwe", "ert"};
	return *(cast (void*) 0);
}



