.PHONY: default
default: main
include arch.mk
include install.mk
include installdeps.mk
include .dir.mk
main: .dir.o
	$(CC) $(LDFLAGS) .dir.o $(LOADLIBES) $(LDLIBS) -o main
clean:
	find . -type f -name '*.o' | xargs rm -vf main
cleanmk:
	find . -type f -name '*.mk' | xargs rm -vf main
run: main
	./main ${ARGS}
valrun: main
	valgrind ./main ${ARGS}
