#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <array.h>

#include "in/in.h"
#include "shared/free.h"

int main()
{
	printf("Hello, World!\n");
	FILE* fin = fopen("example.h", "r");
	struct array* out = cio_in(fin);
	cio_free(&out);
/*	array_delete(&out);*/
	fclose(fin);
	return 0;
}
