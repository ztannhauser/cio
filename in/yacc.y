%{
	#include <stdio.h>
	#include <assert.h>
	
	int yylex();
	int yyerror(char* s);

	struct array* root;

	#define TODO assert(0);

%}

%start root

%define parse.error verbose

%union {
	char ident[20];
}

%token<ident> IDENT;

%token INCLUDE STRING_LITERAL ARROW_STRING_LITERAL RETURN DOUBLE_LITERAL
%token UNION STRUCT SIZEOF RIGHTRIGHTEQUALS RIGHTRIGHT PLUSPLUS CONTINUE
%token XOREQUALS TYPEDEF SINGLELINECOMMENT PLUSEQUALS OREQUALS CHAR_LITERAL
%token MULTILINECOMMENT  TIMESEQUALS OROR NOTEQUALS MINUSMINUS BREAK
%token LEFTLEFTEQUALS LESSEQUALS LEFTLEFT EQUALSEQUALS DIVIDEEQUALS CASE
%token MODEQUALS ANDAND ANDEQUALS ARROW ELLIPSIS GREATEREQUALS DEFAULT
%token MINUSEQUALS CAST WHILE FOR INTEGER_LITERAL EXTERN ENUM SWITCH

%% 

A:
	IDENT {printf("yacc: A: IDENT (%s)\n", $1);}
	| IDENT '[' INTEGER_LITERAL ']' {printf("yacc: A []\n");}
	| '*' A {printf("yacc: * A\n");}
;

include:
	  INCLUDE STRING_LITERAL
	  	{printf("yacc: INCLUDE STRING_LITERAL\n");}
	| INCLUDE ARROW_STRING_LITERAL
		 {printf("yacc: INCLUDE ARROW_STRING_LITERAL\n");}
;

struct_type_C:
	A {printf("yacc: struct_type_C: A\n");}
	| A ',' struct_type_C {printf("yacc: struct_type_C: A ,\n");}
;

struct_type_B:
	type struct_type_C ';'             {printf("yacc: struct_type_B: type\n");}
	| STRUCT '{' struct_type_A '}' ';' {printf("yacc: struct_type_B: struct\n");}
	| UNION '{' struct_type_A '}' ';'  {printf("yacc: struct_type_B: union\n");}
;

struct_type_A:
	struct_type_B
	| struct_type_B struct_type_A
;

d_type:
	STRUCT IDENT '{' struct_type_A '}'  {printf("yacc: d_type: struct\n");}
	| UNION IDENT '{' struct_type_A '}' {printf("yacc: d_type: union\n");}
	| ENUM IDENT '{' enum_type_A '}'    {printf("yacc: d_type: enum\n");}
;

struct_type:
	STRUCT IDENT 
	| STRUCT '{' struct_type_A '}' {printf("yacc: struct_type\n");}
;

union_type:
	UNION IDENT
	| UNION '{' struct_type_A '}' {printf("yacc: union_type\n");}
;

enum_type_B:
	IDENT
	| IDENT '=' INTEGER_LITERAL
;

enum_type_A:
	enum_type_B
	| enum_type_B ',' enum_type_A
;

enum_type:
	ENUM IDENT
	| ENUM '{' enum_type_A '}'
;

type:
	IDENT {printf("yacc: type: IDENT (%s)\n", $1);}
	| struct_type {printf("yacc: type: struct_type\n");}
	| union_type {printf("yacc: type: union_type\n");}
	| enum_type {printf("yacc: type: enum_type\n");}
	| d_type
;

type2:
	type | type2 '*'
;

function_B: type A;

function_A:
	| function_B
	| function_B ',' function_A
	| function_B ',' ELLIPSIS
;

function:
	  type IDENT '(' function_A ')' ';'
	  	 {printf("yacc: function\n");}
	  | type IDENT '(' function_A ')' '{' declares '}'
	  	 {printf("yacc: function with body\n");}
;

global_B:
	A {printf("yacc: global_B: just name\n");}
	| A '=' exprM
	| A '=' '{' list '}'
;

global_A:
	global_B {printf("yacc: global_A: base\n");}
	| global_B ',' global_A {printf("yacc: global_A: additional\n");};


global:
	type global_A {printf("yacc: global\n");}
	| EXTERN type global_A {printf("yacc: extern global\n");}
;

list: exprM | exprM ',' list ;

function_call: exprA '(' list ')' ;

exprA:
	  IDENT
	| CHAR_LITERAL
	| INTEGER_LITERAL
	| DOUBLE_LITERAL
	| STRING_LITERAL
	| '(' exprZ ')'
	| exprA PLUSPLUS
	| exprA MINUSMINUS
	| function_call
	| exprA '[' exprZ ']'
	| exprA '.' IDENT
	| exprA ARROW IDENT
	| CAST '(' type2 ')' '{' list '}'
;

exprB:
	exprA
	| PLUSPLUS exprA
	| MINUSMINUS exprA
	| '+' exprA
	| '-' exprA
	| '!' exprA
	| '~' exprA
	| CAST '(' type2 ')' exprA
	| '*' exprB
	| '&' exprA
	| SIZEOF exprA
;

exprC:
	exprB
	| exprB '*' exprC
	| exprB '/' exprC
	| exprB '%' exprC
;

exprD:
	exprC
	| exprC '+' exprD
	| exprC '-' exprD
;

exprE:
	exprD
	| exprD LEFTLEFT exprE
	| exprD RIGHTRIGHT exprE
;

exprF:
	exprE
	| exprE '<' exprF
	| exprE LESSEQUALS exprF
	| exprE '>' exprF
	| exprE GREATEREQUALS exprF
;

exprG:
	exprF
	| exprF EQUALSEQUALS exprG
	| exprF NOTEQUALS exprG
;

exprH: exprG | exprG '&' exprG ;

exprI: exprH | exprH '^' exprH ;

exprJ: exprI | exprI '|' exprI;

exprK: exprJ | exprJ ANDAND exprJ;

exprL: exprK | exprK OROR exprK ;

exprM:
	exprL
	| exprL '?' ':' exprL
	| exprL '?' exprL ':' exprL
;

assignment:
	  exprB '=' exprN
	| exprB PLUSEQUALS exprN
	| exprB MINUSEQUALS exprN
	| exprB TIMESEQUALS exprN
	| exprB DIVIDEEQUALS exprN
	| exprB MODEQUALS exprN
	| exprB LEFTLEFTEQUALS exprN
	| exprB RIGHTRIGHTEQUALS exprN
	| exprB ANDEQUALS exprN
	| exprB XOREQUALS exprN
	| exprB OREQUALS exprN
;

exprN: exprM | assignment ;

exprZ: exprN | exprN ',' exprZ ;

expr:
	exprA 
	| assignment 
;

while: WHILE '(' exprZ ')' declare;

for_B: | exprZ;

for_A: | global;

for: FOR '(' for_A ';' for_B ';' for_B ')' declare;

switch_C: CHAR_LITERAL | INTEGER_LITERAL;
	
switch_B:
	CASE switch_C ':' declare
	| CASE switch_C ':'
	| DEFAULT ':' declare
;

switch_A: switch_B | switch_B switch_A;

switch:
	SWITCH '(' exprZ ')' '{'
		switch_A
	'}'
;

typedef: TYPEDEF type IDENT ';' ;

comment:
	SINGLELINECOMMENT
	| MULTILINECOMMENT
;

declare:
	include            {printf("yacc: declare: include\n");}
	| function         {printf("yacc: declare: function\n");}
	| global ';'       {printf("yacc: declare: global\n");}
	| expr ';'         {printf("yacc: declare: expr\n");}
	| while            {printf("yacc: declare: while\n");}
	| for              {printf("yacc: declare: for\n");}
	| switch           {printf("yacc: declare: switch\n");}
	| typedef          {printf("yacc: declare: typedef\n");}
	| d_type ';'       {printf("yacc: declare: d_type\n");}
	| comment          {printf("yacc: declare: comment\n");}
	| CONTINUE ';'     {printf("yacc: declare: continue\n");}
	| BREAK ';'        {printf("yacc: declare: break\n");}
	| RETURN ';'       {printf("yacc: declare: return novalue\n");}
	| RETURN exprZ ';' {printf("yacc: declare: return value\n");}
	| '{' declares '}' {printf("yacc: declare: declares\n");}
;

declares:
	declare
	| declare declares
;

root: declares {
//		root = $1;
	};

%%

int yyerror(char* s)
{
  fprintf(stderr, "%s\n",s);
  return 1;
}

int yywrap()
{
  return 1;
} 
