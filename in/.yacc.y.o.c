/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "./in/yacc.y" /* yacc.c:339  */

	#include <stdio.h>
	#include <assert.h>
	
	int yylex();
	int yyerror(char* s);

	struct array* root;

	#define TODO assert(0);


#line 79 "./in/.yacc.y.o.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENT = 258,
    INCLUDE = 259,
    STRING_LITERAL = 260,
    ARROW_STRING_LITERAL = 261,
    RETURN = 262,
    DOUBLE_LITERAL = 263,
    UNION = 264,
    STRUCT = 265,
    SIZEOF = 266,
    RIGHTRIGHTEQUALS = 267,
    RIGHTRIGHT = 268,
    PLUSPLUS = 269,
    CONTINUE = 270,
    XOREQUALS = 271,
    TYPEDEF = 272,
    SINGLELINECOMMENT = 273,
    PLUSEQUALS = 274,
    OREQUALS = 275,
    CHAR_LITERAL = 276,
    MULTILINECOMMENT = 277,
    TIMESEQUALS = 278,
    OROR = 279,
    NOTEQUALS = 280,
    MINUSMINUS = 281,
    BREAK = 282,
    LEFTLEFTEQUALS = 283,
    LESSEQUALS = 284,
    LEFTLEFT = 285,
    EQUALSEQUALS = 286,
    DIVIDEEQUALS = 287,
    CASE = 288,
    MODEQUALS = 289,
    ANDAND = 290,
    ANDEQUALS = 291,
    ARROW = 292,
    ELLIPSIS = 293,
    GREATEREQUALS = 294,
    DEFAULT = 295,
    MINUSEQUALS = 296,
    CAST = 297,
    WHILE = 298,
    FOR = 299,
    INTEGER_LITERAL = 300,
    EXTERN = 301,
    ENUM = 302,
    SWITCH = 303
  };
#endif
/* Tokens.  */
#define IDENT 258
#define INCLUDE 259
#define STRING_LITERAL 260
#define ARROW_STRING_LITERAL 261
#define RETURN 262
#define DOUBLE_LITERAL 263
#define UNION 264
#define STRUCT 265
#define SIZEOF 266
#define RIGHTRIGHTEQUALS 267
#define RIGHTRIGHT 268
#define PLUSPLUS 269
#define CONTINUE 270
#define XOREQUALS 271
#define TYPEDEF 272
#define SINGLELINECOMMENT 273
#define PLUSEQUALS 274
#define OREQUALS 275
#define CHAR_LITERAL 276
#define MULTILINECOMMENT 277
#define TIMESEQUALS 278
#define OROR 279
#define NOTEQUALS 280
#define MINUSMINUS 281
#define BREAK 282
#define LEFTLEFTEQUALS 283
#define LESSEQUALS 284
#define LEFTLEFT 285
#define EQUALSEQUALS 286
#define DIVIDEEQUALS 287
#define CASE 288
#define MODEQUALS 289
#define ANDAND 290
#define ANDEQUALS 291
#define ARROW 292
#define ELLIPSIS 293
#define GREATEREQUALS 294
#define DEFAULT 295
#define MINUSEQUALS 296
#define CAST 297
#define WHILE 298
#define FOR 299
#define INTEGER_LITERAL 300
#define EXTERN 301
#define ENUM 302
#define SWITCH 303

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 18 "./in/yacc.y" /* yacc.c:355  */

	char ident[20];

#line 216 "./in/.yacc.y.o.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 233 "./in/.yacc.y.o.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  127
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   518

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  73
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  52
/* YYNRULES -- Number of rules.  */
#define YYNRULES  157
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  303

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   303

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    62,     2,     2,     2,    66,    64,     2,
      57,    58,    51,    60,    52,    61,    59,    65,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    72,    53,
      67,    56,    68,    71,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    49,     2,    50,    69,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    54,    70,    55,    63,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    35,    35,    36,    37,    41,    43,    48,    49,    53,
      54,    55,    59,    60,    64,    65,    66,    70,    71,    75,
      76,    80,    81,    85,    86,    90,    91,    95,    96,    97,
      98,    99,   103,   103,   106,   108,   109,   110,   111,   115,
     117,   122,   123,   124,   128,   129,   133,   134,   137,   137,
     139,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   172,   173,   174,   175,   179,
     180,   181,   185,   186,   187,   191,   192,   193,   194,   195,
     199,   200,   201,   204,   204,   206,   206,   208,   208,   210,
     210,   212,   212,   215,   216,   217,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   234,   234,   236,
     236,   239,   240,   243,   245,   245,   247,   247,   249,   251,
     251,   254,   255,   256,   259,   259,   262,   267,   270,   271,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   293,   294,   297
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IDENT", "INCLUDE", "STRING_LITERAL",
  "ARROW_STRING_LITERAL", "RETURN", "DOUBLE_LITERAL", "UNION", "STRUCT",
  "SIZEOF", "RIGHTRIGHTEQUALS", "RIGHTRIGHT", "PLUSPLUS", "CONTINUE",
  "XOREQUALS", "TYPEDEF", "SINGLELINECOMMENT", "PLUSEQUALS", "OREQUALS",
  "CHAR_LITERAL", "MULTILINECOMMENT", "TIMESEQUALS", "OROR", "NOTEQUALS",
  "MINUSMINUS", "BREAK", "LEFTLEFTEQUALS", "LESSEQUALS", "LEFTLEFT",
  "EQUALSEQUALS", "DIVIDEEQUALS", "CASE", "MODEQUALS", "ANDAND",
  "ANDEQUALS", "ARROW", "ELLIPSIS", "GREATEREQUALS", "DEFAULT",
  "MINUSEQUALS", "CAST", "WHILE", "FOR", "INTEGER_LITERAL", "EXTERN",
  "ENUM", "SWITCH", "'['", "']'", "'*'", "','", "';'", "'{'", "'}'", "'='",
  "'('", "')'", "'.'", "'+'", "'-'", "'!'", "'~'", "'&'", "'/'", "'%'",
  "'<'", "'>'", "'^'", "'|'", "'?'", "':'", "$accept", "A", "include",
  "struct_type_C", "struct_type_B", "struct_type_A", "d_type",
  "struct_type", "union_type", "enum_type_B", "enum_type_A", "enum_type",
  "type", "type2", "function_B", "function_A", "function", "global_B",
  "global_A", "global", "list", "function_call", "exprA", "exprB", "exprC",
  "exprD", "exprE", "exprF", "exprG", "exprH", "exprI", "exprJ", "exprK",
  "exprL", "exprM", "assignment", "exprN", "exprZ", "expr", "while",
  "for_B", "for_A", "for", "switch_C", "switch_B", "switch_A", "switch",
  "typedef", "comment", "declare", "declares", "root", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,    91,
      93,    42,    44,    59,   123,   125,    61,    40,    41,    46,
      43,    45,    33,   126,    38,    47,    37,    60,    62,    94,
     124,    63,    58
};
# endif

#define YYPACT_NINF -195

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-195)))

#define YYTABLE_NINF -122

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     316,     6,   124,  -195,   378,  -195,    12,    13,    51,    51,
     -11,   125,  -195,  -195,  -195,    51,    -6,    -2,    44,    48,
    -195,   125,    14,    53,   454,   316,   454,    51,    51,    51,
      51,    51,  -195,    24,  -195,  -195,  -195,    18,  -195,    74,
    -195,   130,   451,  -195,    88,  -195,  -195,  -195,  -195,  -195,
     316,  -195,    63,  -195,  -195,  -195,  -195,   149,   223,    78,
       9,    45,    75,   -13,    28,    73,   135,   136,   114,  -195,
    -195,   147,   166,   146,   181,   172,   181,   170,   149,   149,
    -195,  -195,  -195,   226,   149,  -195,   125,   454,   122,    40,
     180,   233,   454,  -195,   182,   183,   149,   149,   149,   149,
     149,  -195,    -9,    40,   188,   186,  -195,  -195,  -195,  -195,
     242,   454,   454,   244,   454,   454,   454,   454,   454,   454,
     454,   454,   454,   454,   454,  -195,  -195,  -195,   454,   454,
     454,   454,   454,   454,   454,   454,   454,   454,   454,   454,
     454,   454,   454,   454,   454,   454,   150,   454,  -195,   181,
      26,    35,   181,   193,    40,   181,   194,   125,   199,  -195,
       7,   195,    40,  -195,   201,   207,  -195,   233,   202,   208,
     210,   204,  -195,  -195,   218,   125,  -195,   392,    40,  -195,
     216,   209,   131,   219,  -195,  -195,  -195,  -195,  -195,  -195,
    -195,  -195,  -195,  -195,  -195,  -195,  -195,  -195,  -195,  -195,
    -195,  -195,  -195,  -195,  -195,  -195,  -195,  -195,  -195,  -195,
    -195,  -195,  -195,  -195,   454,   196,  -195,   215,   181,   181,
    -195,  -195,   220,   222,   225,  -195,    82,  -195,  -195,    41,
     316,   454,   227,   228,   233,  -195,   224,   231,    40,   234,
     232,   454,  -195,  -195,  -195,  -195,   454,  -195,   454,  -195,
     236,   237,    40,  -195,  -195,   229,   454,   149,  -195,  -195,
     246,  -195,  -195,  -195,    31,  -195,  -195,   171,    92,   240,
    -195,  -195,   249,   252,  -195,   251,   454,    16,   205,    31,
     254,  -195,  -195,  -195,   316,  -195,  -195,  -195,  -195,   253,
    -195,  -195,   213,   316,  -195,  -195,   255,   316,   316,  -195,
    -195,  -195,  -195
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    51,     0,    55,     0,    54,     0,     0,     0,     0,
       0,     0,   138,    52,   139,     0,     0,     0,     0,     0,
      53,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   140,    31,    28,    29,    30,     0,   141,     0,
      59,    64,     0,   122,     0,   144,   145,   146,   147,   149,
     155,   157,     0,     5,     6,    51,   152,    64,    75,    79,
      82,    85,    90,    93,    95,    97,    99,   101,   103,   117,
     118,   119,     0,    19,     0,    17,     0,     0,    74,    65,
     150,    27,    31,     0,    66,   151,     0,     0,   126,     0,
      25,     0,     0,    72,     0,     0,    67,    68,    69,    70,
      73,   148,     2,     0,    41,    44,    46,   142,    57,    58,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   143,   156,     1,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   153,     0,
       0,     0,    12,     0,     0,     0,     0,     0,     0,    32,
       0,     0,     0,   127,     0,     2,    47,     0,    21,    23,
       0,     0,   154,    56,     0,    35,     4,     0,     0,    62,
       0,     0,    75,    48,    61,   113,   115,   107,   116,   109,
     112,   110,   111,   114,   108,   106,    76,    77,    78,    80,
      81,    84,    83,    87,    89,    86,    88,    92,    91,    94,
      96,    98,   100,   102,     0,     0,   120,     0,     0,     0,
      13,    20,     7,     0,     0,    18,     0,   137,    33,     0,
       0,   124,     0,     0,     0,    26,     0,     0,     0,    36,
       0,     0,    42,    45,    60,    50,     0,   104,     0,    15,
       0,     0,     0,     9,    14,     0,     0,    71,   123,   125,
       0,    16,    22,    24,     0,     3,    34,    35,     0,     0,
      49,   105,    20,    18,     8,     0,   124,     0,     0,   134,
       0,    38,    37,    39,     0,    43,    11,    10,    63,     0,
     129,   130,     0,     0,   135,   136,     0,     0,   132,   133,
      40,   128,   131
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -195,  -102,  -195,    62,  -195,   -70,    10,  -195,  -195,  -195,
    -153,  -195,     2,   155,  -195,    49,  -195,  -195,   -84,   241,
     -68,  -195,     3,     0,    87,    34,    60,    67,    84,   173,
     179,   184,   187,  -139,  -104,    20,   230,    15,  -195,  -195,
      59,  -195,  -195,  -195,  -195,    57,  -195,  -195,  -195,  -194,
     -23,  -195
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,   104,    32,   223,   152,   153,    82,    34,    35,   169,
     170,    36,    37,   160,   239,   240,    38,   105,   106,    39,
     181,    40,    57,   182,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,   259,    44,    45,
     260,   164,    46,   292,   279,   280,    47,    48,    49,    50,
      51,    52
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      42,   176,    94,    41,    58,   166,   156,   215,   183,   -27,
      33,    78,    79,    83,   232,    73,    75,    90,    84,    72,
      43,   102,   133,    89,    93,    42,    58,   126,    41,    73,
      96,    97,    98,    99,   100,    33,   258,   290,    75,   134,
     174,    95,    80,   165,    55,    43,     3,    85,   175,     5,
      42,   141,   222,    41,    55,    86,     3,   -27,   228,     5,
      33,   291,    13,   127,   277,   229,    74,    76,    91,   103,
      43,   278,    13,   242,   135,   247,   154,   101,   154,   217,
     218,   263,   220,    77,   136,   224,    20,    58,   159,   219,
     162,   103,    58,    77,   243,   256,    20,   142,    26,   299,
     139,    87,   161,   301,   302,    88,   140,   171,    26,   271,
      92,    58,   137,   138,    58,    58,    58,    58,    58,    58,
      58,    58,    58,    58,    58,    81,   180,   107,    81,    53,
      54,     6,     7,   228,     6,     7,   266,   183,   131,   132,
     255,   125,   183,   143,   108,   283,   284,    58,   250,   251,
     222,   154,   183,    55,   154,     3,   109,   154,     5,   159,
     145,     8,   216,   108,     9,   199,   200,   110,    21,    22,
     144,    13,    22,   269,    81,   109,    15,   238,   270,   111,
       6,     7,   128,  -121,    81,   146,   110,   112,   275,   113,
     150,   151,    17,   201,   202,    20,   129,   130,   111,   147,
     149,    24,   203,   204,   205,   206,   112,    26,   113,   281,
      27,    28,    29,    30,    31,   196,   197,   198,    22,   148,
     154,   154,   214,   207,   208,   209,   155,   157,    22,   158,
      42,    58,   257,    41,   167,   114,   168,   172,   178,   115,
      33,   173,   116,   117,   177,   179,   118,   184,   221,   225,
      43,   119,   227,   230,   231,   120,   174,   121,   233,   122,
     234,   296,   236,   237,   123,   235,   244,   245,   248,   238,
     249,   246,   252,   262,   128,   253,    58,   293,   264,   124,
     254,   265,   261,   256,    42,   298,   267,    41,   129,   130,
     268,   272,   273,    42,    33,   285,    41,    42,    42,   276,
      41,    41,   286,    33,    43,   287,   288,    33,    33,   295,
     300,   297,   226,    43,   274,   210,   282,    43,    43,     1,
       2,     3,   211,     4,     5,     6,     7,     8,   212,   163,
       9,    10,   213,    11,    12,   289,   294,    13,    14,     0,
       0,     0,    15,    16,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,     0,     0,     0,    17,    18,
      19,    20,    21,    22,    23,     0,     0,    24,     0,     0,
      25,     0,     0,    26,     0,     0,    27,    28,    29,    30,
      31,    55,     0,     3,     0,     0,     5,     0,     0,     8,
       0,     0,     9,     0,     0,    55,     0,     3,     0,    13,
       5,     0,     0,     8,    15,     0,     9,     0,     0,     0,
       0,     0,     0,    13,     0,     0,     0,     0,    15,     0,
      17,     0,     0,    20,     0,     0,     0,     0,     0,    24,
       0,    56,     0,     0,    17,    26,     0,    20,    27,    28,
      29,    30,    31,    24,     0,     0,   241,     0,     0,    26,
       0,     0,    27,    28,    29,    30,    31,    55,     0,     3,
       0,     0,     5,   114,     0,     8,     0,   115,     9,     0,
     116,   117,     0,     0,   118,    13,     0,     0,     0,   119,
      15,     0,     0,   120,     0,   121,     0,   122,     0,     0,
       0,     0,   123,     0,     0,     0,    17,     0,     0,    20,
       0,     0,     0,     0,     0,    24,     0,   124,     0,     0,
       0,    26,     0,     0,    27,    28,    29,    30,    31
};

static const yytype_int16 yycheck[] =
{
       0,   103,    25,     0,     4,    89,    76,   146,   112,     3,
       0,     8,     9,    11,   167,     3,     3,     3,    15,     4,
       0,     3,    13,    21,    24,    25,    26,    50,    25,     3,
      27,    28,    29,    30,    31,    25,   230,    21,     3,    30,
      49,    26,    53,     3,     3,    25,     5,    53,    57,     8,
      50,    64,   154,    50,     3,    57,     5,    51,    51,     8,
      50,    45,    21,     0,    33,    58,    54,    54,    54,    51,
      50,    40,    21,   177,    29,   214,    74,    53,    76,   149,
      54,   234,   152,    42,    39,   155,    45,    87,    86,    54,
      88,    51,    92,    42,   178,    54,    45,    69,    57,   293,
      25,    57,    87,   297,   298,    57,    31,    92,    57,   248,
      57,   111,    67,    68,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,     3,   111,    53,     3,     5,
       6,     9,    10,    51,     9,    10,   238,   241,    60,    61,
      58,    53,   246,    70,    14,    53,    54,   147,   218,   219,
     252,   149,   256,     3,   152,     5,    26,   155,     8,   157,
      24,    11,   147,    14,    14,   131,   132,    37,    46,    47,
      35,    21,    47,   241,     3,    26,    26,   175,   246,    49,
       9,    10,    51,    53,     3,    71,    37,    57,   256,    59,
       9,    10,    42,   133,   134,    45,    65,    66,    49,    52,
      54,    51,   135,   136,   137,   138,    57,    57,    59,    38,
      60,    61,    62,    63,    64,   128,   129,   130,    47,    53,
     218,   219,    72,   139,   140,   141,    54,    57,    47,     3,
     230,   231,   229,   230,    54,    12,     3,    55,    52,    16,
     230,    58,    19,    20,    56,     3,    23,     3,    55,    55,
     230,    28,    53,    58,    53,    32,    49,    34,    56,    36,
      52,   284,    58,    45,    41,    55,    50,    58,    72,   267,
      55,    52,    52,    45,    51,    53,   276,    72,    54,    56,
      55,    50,    55,    54,   284,    72,    52,   284,    65,    66,
      58,    55,    55,   293,   284,    55,   293,   297,   298,    53,
     297,   298,    53,   293,   284,    53,    55,   297,   298,    55,
      55,    58,   157,   293,   252,   142,   267,   297,   298,     3,
       4,     5,   143,     7,     8,     9,    10,    11,   144,    88,
      14,    15,   145,    17,    18,   276,   279,    21,    22,    -1,
      -1,    -1,    26,    27,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,    -1,    -1,    -1,    42,    43,
      44,    45,    46,    47,    48,    -1,    -1,    51,    -1,    -1,
      54,    -1,    -1,    57,    -1,    -1,    60,    61,    62,    63,
      64,     3,    -1,     5,    -1,    -1,     8,    -1,    -1,    11,
      -1,    -1,    14,    -1,    -1,     3,    -1,     5,    -1,    21,
       8,    -1,    -1,    11,    26,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    -1,    21,    -1,    -1,    -1,    -1,    26,    -1,
      42,    -1,    -1,    45,    -1,    -1,    -1,    -1,    -1,    51,
      -1,    53,    -1,    -1,    42,    57,    -1,    45,    60,    61,
      62,    63,    64,    51,    -1,    -1,    54,    -1,    -1,    57,
      -1,    -1,    60,    61,    62,    63,    64,     3,    -1,     5,
      -1,    -1,     8,    12,    -1,    11,    -1,    16,    14,    -1,
      19,    20,    -1,    -1,    23,    21,    -1,    -1,    -1,    28,
      26,    -1,    -1,    32,    -1,    34,    -1,    36,    -1,    -1,
      -1,    -1,    41,    -1,    -1,    -1,    42,    -1,    -1,    45,
      -1,    -1,    -1,    -1,    -1,    51,    -1,    56,    -1,    -1,
      -1,    57,    -1,    -1,    60,    61,    62,    63,    64
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     7,     8,     9,    10,    11,    14,
      15,    17,    18,    21,    22,    26,    27,    42,    43,    44,
      45,    46,    47,    48,    51,    54,    57,    60,    61,    62,
      63,    64,    75,    79,    80,    81,    84,    85,    89,    92,
      94,    95,    96,   108,   111,   112,   115,   119,   120,   121,
     122,   123,   124,     5,     6,     3,    53,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,     3,    54,     3,    54,    42,    95,    95,
      53,     3,    79,    85,    95,    53,    57,    57,    57,    85,
       3,    54,    57,    96,   123,   110,    95,    95,    95,    95,
      95,    53,     3,    51,    74,    90,    91,    53,    14,    26,
      37,    49,    57,    59,    12,    16,    19,    20,    23,    28,
      32,    34,    36,    41,    56,    53,   123,     0,    51,    65,
      66,    60,    61,    13,    30,    29,    39,    67,    68,    25,
      31,    64,    69,    70,    35,    24,    71,    52,    53,    54,
       9,    10,    77,    78,    85,    54,    78,    57,     3,    85,
      86,   110,    85,    92,   114,     3,    91,    54,     3,    82,
      83,   110,    55,    58,    49,    57,    74,    56,    52,     3,
     110,    93,    96,   107,     3,   109,   109,   109,   109,   109,
     109,   109,   109,   109,   109,   109,    97,    97,    97,    98,
      98,    99,    99,   100,   100,   100,   100,   101,   101,   101,
     102,   103,   104,   105,    72,   106,   110,    78,    54,    54,
      78,    55,    74,    76,    78,    55,    86,    53,    51,    58,
      58,    53,    83,    56,    52,    55,    58,    45,    85,    87,
      88,    54,   107,    91,    50,    58,    52,   106,    72,    55,
      78,    78,    52,    53,    55,    58,    54,    95,   122,   110,
     113,    55,    45,    83,    54,    50,    74,    52,    58,    93,
      93,   106,    55,    55,    76,    93,    53,    33,    40,   117,
     118,    38,    88,    53,    54,    55,    53,    53,    55,   113,
      21,    45,   116,    72,   118,    55,   123,    58,    72,   122,
      55,   122,   122
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    73,    74,    74,    74,    75,    75,    76,    76,    77,
      77,    77,    78,    78,    79,    79,    79,    80,    80,    81,
      81,    82,    82,    83,    83,    84,    84,    85,    85,    85,
      85,    85,    86,    86,    87,    88,    88,    88,    88,    89,
      89,    90,    90,    90,    91,    91,    92,    92,    93,    93,
      94,    95,    95,    95,    95,    95,    95,    95,    95,    95,
      95,    95,    95,    95,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    97,    97,    97,    97,    98,
      98,    98,    99,    99,    99,   100,   100,   100,   100,   100,
     101,   101,   101,   102,   102,   103,   103,   104,   104,   105,
     105,   106,   106,   107,   107,   107,   108,   108,   108,   108,
     108,   108,   108,   108,   108,   108,   108,   109,   109,   110,
     110,   111,   111,   112,   113,   113,   114,   114,   115,   116,
     116,   117,   117,   117,   118,   118,   119,   120,   121,   121,
     122,   122,   122,   122,   122,   122,   122,   122,   122,   122,
     122,   122,   122,   122,   122,   123,   123,   124
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     4,     2,     2,     2,     1,     3,     3,
       5,     5,     1,     2,     5,     5,     5,     2,     4,     2,
       4,     1,     3,     1,     3,     2,     4,     1,     1,     1,
       1,     1,     1,     2,     2,     0,     1,     3,     3,     6,
       8,     1,     3,     5,     1,     3,     2,     3,     1,     3,
       4,     1,     1,     1,     1,     1,     3,     2,     2,     1,
       4,     3,     3,     7,     1,     2,     2,     2,     2,     2,
       2,     5,     2,     2,     2,     1,     3,     3,     3,     1,
       3,     3,     1,     3,     3,     1,     3,     3,     3,     3,
       1,     3,     3,     1,     3,     1,     3,     1,     3,     1,
       3,     1,     3,     1,     4,     5,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     1,     1,     1,
       3,     1,     1,     5,     0,     1,     0,     1,     9,     1,
       1,     4,     3,     3,     1,     2,     7,     4,     1,     1,
       1,     1,     2,     2,     1,     1,     1,     1,     2,     1,
       2,     2,     2,     3,     3,     1,     2,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 35 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: A: IDENT (%s)\n", (yyvsp[0].ident));}
#line 1575 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 3:
#line 36 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: A []\n");}
#line 1581 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 4:
#line 37 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: * A\n");}
#line 1587 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 5:
#line 42 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: INCLUDE STRING_LITERAL\n");}
#line 1593 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 6:
#line 44 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: INCLUDE ARROW_STRING_LITERAL\n");}
#line 1599 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 7:
#line 48 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: struct_type_C: A\n");}
#line 1605 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 8:
#line 49 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: struct_type_C: A ,\n");}
#line 1611 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 9:
#line 53 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: struct_type_B: type\n");}
#line 1617 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 10:
#line 54 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: struct_type_B: struct\n");}
#line 1623 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 11:
#line 55 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: struct_type_B: union\n");}
#line 1629 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 14:
#line 64 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: d_type: struct\n");}
#line 1635 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 15:
#line 65 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: d_type: union\n");}
#line 1641 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 16:
#line 66 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: d_type: enum\n");}
#line 1647 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 18:
#line 71 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: struct_type\n");}
#line 1653 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 20:
#line 76 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: union_type\n");}
#line 1659 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 27:
#line 95 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: type: IDENT (%s)\n", (yyvsp[0].ident));}
#line 1665 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 28:
#line 96 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: type: struct_type\n");}
#line 1671 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 29:
#line 97 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: type: union_type\n");}
#line 1677 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 30:
#line 98 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: type: enum_type\n");}
#line 1683 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 39:
#line 116 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: function\n");}
#line 1689 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 40:
#line 118 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: function with body\n");}
#line 1695 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 41:
#line 122 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: global_B: just name\n");}
#line 1701 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 44:
#line 128 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: global_A: base\n");}
#line 1707 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 45:
#line 129 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: global_A: additional\n");}
#line 1713 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 46:
#line 133 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: global\n");}
#line 1719 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 47:
#line 134 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: extern global\n");}
#line 1725 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 140:
#line 275 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: include\n");}
#line 1731 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 141:
#line 276 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: function\n");}
#line 1737 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 142:
#line 277 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: global\n");}
#line 1743 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 143:
#line 278 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: expr\n");}
#line 1749 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 144:
#line 279 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: while\n");}
#line 1755 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 145:
#line 280 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: for\n");}
#line 1761 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 146:
#line 281 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: switch\n");}
#line 1767 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 147:
#line 282 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: typedef\n");}
#line 1773 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 148:
#line 283 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: d_type\n");}
#line 1779 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 149:
#line 284 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: comment\n");}
#line 1785 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 150:
#line 285 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: continue\n");}
#line 1791 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 151:
#line 286 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: break\n");}
#line 1797 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 152:
#line 287 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: return novalue\n");}
#line 1803 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 153:
#line 288 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: return value\n");}
#line 1809 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 154:
#line 289 "./in/yacc.y" /* yacc.c:1646  */
    {printf("yacc: declare: declares\n");}
#line 1815 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;

  case 157:
#line 297 "./in/yacc.y" /* yacc.c:1646  */
    {
//		root = $1;
	}
#line 1823 "./in/.yacc.y.o.c" /* yacc.c:1646  */
    break;


#line 1827 "./in/.yacc.y.o.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 301 "./in/yacc.y" /* yacc.c:1906  */


int yyerror(char* s)
{
  fprintf(stderr, "%s\n",s);
  return 1;
}

int yywrap()
{
  return 1;
} 
