/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_IN_Y_TAB_H_INCLUDED
# define YY_YY_IN_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENT = 258,
    INCLUDE = 259,
    STRING_LITERAL = 260,
    ARROW_STRING_LITERAL = 261,
    RETURN = 262,
    DOUBLE_LITERAL = 263,
    UNION = 264,
    STRUCT = 265,
    SIZEOF = 266,
    RIGHTRIGHTEQUALS = 267,
    RIGHTRIGHT = 268,
    PLUSPLUS = 269,
    CONTINUE = 270,
    XOREQUALS = 271,
    TYPEDEF = 272,
    SINGLELINECOMMENT = 273,
    PLUSEQUALS = 274,
    OREQUALS = 275,
    CHAR_LITERAL = 276,
    MULTILINECOMMENT = 277,
    TIMESEQUALS = 278,
    OROR = 279,
    NOTEQUALS = 280,
    MINUSMINUS = 281,
    BREAK = 282,
    LEFTLEFTEQUALS = 283,
    LESSEQUALS = 284,
    LEFTLEFT = 285,
    EQUALSEQUALS = 286,
    DIVIDEEQUALS = 287,
    CASE = 288,
    MODEQUALS = 289,
    ANDAND = 290,
    ANDEQUALS = 291,
    ARROW = 292,
    ELLIPSIS = 293,
    GREATEREQUALS = 294,
    DEFAULT = 295,
    MINUSEQUALS = 296,
    CAST = 297,
    WHILE = 298,
    FOR = 299,
    INTEGER_LITERAL = 300,
    EXTERN = 301,
    ENUM = 302,
    SWITCH = 303
  };
#endif
/* Tokens.  */
#define IDENT 258
#define INCLUDE 259
#define STRING_LITERAL 260
#define ARROW_STRING_LITERAL 261
#define RETURN 262
#define DOUBLE_LITERAL 263
#define UNION 264
#define STRUCT 265
#define SIZEOF 266
#define RIGHTRIGHTEQUALS 267
#define RIGHTRIGHT 268
#define PLUSPLUS 269
#define CONTINUE 270
#define XOREQUALS 271
#define TYPEDEF 272
#define SINGLELINECOMMENT 273
#define PLUSEQUALS 274
#define OREQUALS 275
#define CHAR_LITERAL 276
#define MULTILINECOMMENT 277
#define TIMESEQUALS 278
#define OROR 279
#define NOTEQUALS 280
#define MINUSMINUS 281
#define BREAK 282
#define LEFTLEFTEQUALS 283
#define LESSEQUALS 284
#define LEFTLEFT 285
#define EQUALSEQUALS 286
#define DIVIDEEQUALS 287
#define CASE 288
#define MODEQUALS 289
#define ANDAND 290
#define ANDEQUALS 291
#define ARROW 292
#define ELLIPSIS 293
#define GREATEREQUALS 294
#define DEFAULT 295
#define MINUSEQUALS 296
#define CAST 297
#define WHILE 298
#define FOR 299
#define INTEGER_LITERAL 300
#define EXTERN 301
#define ENUM 302
#define SWITCH 303

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 19 "in/yacc.y" /* yacc.c:1909  */

	char ident[20];

#line 154 "in/y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_IN_Y_TAB_H_INCLUDED  */
