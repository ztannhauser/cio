#include <stdio.h>

#include <array.h>

extern FILE* yyin;
extern struct array* root;

int yyparse();

struct array* cio_in(FILE* in)
{
	yyin = in;
	yyparse();
	return root;
}
