%{
	#include <stdio.h>
	#include "y.tab.h"
	int c;
%}
%x comment
%%
[ \t\r\n] ;

"//".* {
	return SINGLELINECOMMENT;
}

"/*" {
	BEGIN(comment);
}

<comment>"*/" {
	BEGIN(INITIAL);
	return MULTILINECOMMENT;
}

<comment>. {
	
}

"++" {
	return PLUSPLUS;
}

"--" {
	return MINUSMINUS;
}

"&&" {
	return ANDAND;
}

">>" {
	return RIGHTRIGHT;
}

"<<" {
	return LEFTLEFT;
}

"+=" {
	return PLUSEQUALS;
}

"-=" {
	return MINUSEQUALS;
}

"*=" {
	return TIMESEQUALS;
}

"/=" {
	return DIVIDEEQUALS;
}

"%=" {
	return MODEQUALS;
}

">=" {
	return LESSEQUALS;
}

"<=" {
	return GREATEREQUALS;
}

"==" {
	return EQUALSEQUALS;
}

"!=" {
	return NOTEQUALS;
}

"..." {
	return ELLIPSIS;
}

"for" {
	return FOR;
}

"while" {
	return WHILE;
}

"return" {
	return RETURN;
}

"struct" {
	return STRUCT;
}

"union" {
	return UNION;
}

"enum" {
	return ENUM;
}

"typedef" {
	return TYPEDEF;
}

"#include" {
	return INCLUDE;
}

"extern" {
	return EXTERN;
}

"switch" {
	return SWITCH;
}

"case" {
	return CASE;
}

"default" {
	return DEFAULT;
}

"cast" {
	return CAST;
}

\<.+\> {
	return ARROW_STRING_LITERAL;
}

\".+\" {
	return STRING_LITERAL;
}

"'"."'" {
	return CHAR_LITERAL;
}

0x[0-9]+ {
	return INTEGER_LITERAL;
}

[0-9]+ {
	return INTEGER_LITERAL;
}

[0-9]+.[0-9]+ {
	return DOUBLE_LITERAL;
}

[A-Za-z_][_A-Za-z0-9]* {
	strcpy(yylval.ident, yytext);
	return IDENT;
}

[][*;,}{)(=:+-/><&%?:] {
	return yytext[0];
}

. {
	c = yytext[0];
	abort();
}
%%
